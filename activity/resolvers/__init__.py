from .query import query
from .status_update import status_update

resolvers = [query, status_update]
